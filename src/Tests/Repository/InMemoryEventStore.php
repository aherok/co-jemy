<?php

namespace Tests\Repository;

use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\Event;
use Domain\CoJemy\EventStorePersister;
use Domain\CoJemy\EventStoreRepository;
use Domain\CoJemy\Order;

class InMemoryEventStore implements EventStorePersister, EventStoreRepository
{
    /**
     * @var Event[]
     */
    private $events = [];

    /**
     * @param Event[] $events
     */
    public function persist(array $events)
    {
        foreach($events as $event) {
            $this->events[] = $event;
        }
    }
    
    public function clear()
    {
        $this->events = [];
    }

    /**
     * @return int
     */
    public function count()
    {
        $temp = [];
        foreach ($this->events as $event) {
            $temp[$event->getParametersBag()->getParameter('aggregateId')] = null;
        }
        return count($temp);
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return Order::recreate($this->events[0]->getParametersBag()->getParameter('aggregateId'), $this->events);
    }

    /**
     * @param AggregateId $id
     * @return Order
     */
    public function findOrderById(AggregateId $id)
    {
        $events = [];

        foreach ($this->events as $event) {
            $orderId = $event->getParametersBag()->getParameter('aggregateId');
            if ($orderId === (string) $id) {
                $events[] = $event;
            }
        }
        $order = Order::recreate((string) $id, $events);

        return $order;
    }
}
