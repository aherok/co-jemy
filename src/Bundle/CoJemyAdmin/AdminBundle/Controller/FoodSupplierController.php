<?php

namespace Bundle\CoJemyAdmin\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Bundle\CoJemyCore\CoreBundle\Entity\FoodSupplier;
use Bundle\CoJemyAdmin\AdminBundle\Form\FoodSupplierType;

/**
 * FoodSupplier controller.
 *
 */
class FoodSupplierController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $foodSuppliers = $em->getRepository('CoreBundle:FoodSupplier')->findAll();

        return $this->render('foodsupplier/index.html.twig', array(
            'foodSuppliers' => $foodSuppliers,
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request)
    {
        $foodSupplier = new FoodSupplier();
        $form = $this->createForm(FoodSupplierType::class, $foodSupplier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($foodSupplier);
            $em->flush();

            return $this->redirectToRoute('foodsupplier_show', ['id' => $foodSupplier->getId()]);
        }

        return $this->render('foodsupplier/new.html.twig', [
            'foodSupplier' => $foodSupplier,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param FoodSupplier $foodSupplier
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(FoodSupplier $foodSupplier)
    {
        return $this->render('foodsupplier/show.html.twig', ['foodSupplier' => $foodSupplier]);
    }

    /**
     * @param Request $request
     * @param FoodSupplier $foodSupplier
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, FoodSupplier $foodSupplier)
    {
        $editForm = $this->createForm(FoodSupplierType::class, $foodSupplier);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($foodSupplier);
            $em->flush();

            return $this->redirectToRoute('foodsupplier_show', array('id' => $foodSupplier->getId()));
        }

        return $this->render('foodsupplier/edit.html.twig', array(
            'foodSupplier' => $foodSupplier,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * @param FoodSupplier $foodSupplier
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(FoodSupplier $foodSupplier)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($foodSupplier);
        $em->flush();

        return $this->redirectToRoute('foodsupplier_index');
    }
}
