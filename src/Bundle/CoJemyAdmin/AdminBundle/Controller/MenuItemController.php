<?php

namespace Bundle\CoJemyAdmin\AdminBundle\Controller;

use Bundle\CoJemyCore\CoreBundle\Entity\FoodSupplier;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Bundle\CoJemyCore\CoreBundle\Entity\MenuItem;
use Bundle\CoJemyAdmin\AdminBundle\Form\MenuItemType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * MenuItem controller.
 *
 */
class MenuItemController extends Controller
{
    /**
     * @param Request $request
     * @param FoodSupplier $foodSupplier
     * @ParamConverter("foodSupplier", class="CoreBundle:FoodSupplier", options={"mapping": {"foodSupplierId": "id"}})
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function newAction(Request $request, FoodSupplier $foodSupplier)
    {
        $menuItem = new MenuItem();
        $menuItem->setFoodSupplier($foodSupplier);
        
        $form = $this->createForm(MenuItemType::class, $menuItem);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($menuItem);
            $em->flush();

            return $this->redirectToRoute('foodsupplier_show', array('id' => $foodSupplier->getId()));
        }

        return $this->render('menuitem/new.html.twig', array(
            'menuItem' => $menuItem,
            'form' => $form->createView(),
        ));
    }

    /**
     * @param Request $request
     * @param FoodSupplier $foodSupplier
     * @param MenuItem $menuItem
     * @ParamConverter("foodSupplier", class="CoreBundle:FoodSupplier", options={"mapping": {"foodSupplierId": "id"}})
     * @ParamConverter("menuItem", class="CoreBundle:MenuItem", options={"mapping": {"menuItemId": "id"}})
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, FoodSupplier $foodSupplier, MenuItem $menuItem)
    {
        $editForm = $this->createForm(MenuItemType::class, $menuItem);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($menuItem);
            $em->flush();

            return $this->redirectToRoute('foodsupplier_show', array('id' => $foodSupplier->getId()));
        }

        return $this->render('menuitem/edit.html.twig', array(
            'menuItem' => $menuItem,
            'edit_form' => $editForm->createView()
        ));
    }

    /**
     * @param Request $request
     * @param FoodSupplier $foodSupplier
     * @param MenuItem $menuItem
     * @ParamConverter("foodSupplier", class="CoreBundle:FoodSupplier", options={"mapping": {"foodSupplierId": "id"}})
     * @ParamConverter("menuItem", class="CoreBundle:MenuItem", options={"mapping": {"menuItemId": "id"}})
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, FoodSupplier $foodSupplier, MenuItem $menuItem)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($menuItem);
        $em->flush();

        return $this->redirectToRoute('foodsupplier_show', ['id' => $foodSupplier->getId()]);
    }
}
