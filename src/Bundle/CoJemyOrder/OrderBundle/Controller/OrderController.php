<?php

namespace Bundle\CoJemyOrder\OrderBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class OrderController extends Controller
{
    public function indexAction()
    {
        return $this->render('order\index.html.twig');
    }
}
