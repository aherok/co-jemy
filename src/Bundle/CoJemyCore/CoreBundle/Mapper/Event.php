<?php

namespace Bundle\CoJemyCore\CoreBundle\Mapper;

use Domain\CoJemy\Event as DomainEvent;
use Domain\CoJemy\Order\EventFactory;

use Bundle\CoJemyCore\CoreBundle\Entity\Event as EntityEvent;

class Event
{
    /**
     * @param DomainEvent $event
     *
     * @return EntityEvent
     */
    public function mapDomainToEntity(DomainEvent $event)
    {
        $eventEntity = new EntityEvent();

        $eventEntity->setAggregateId($event->getParametersBag()->getParameter('aggregateId'));
        $eventEntity->setDateCreated(new \DateTime());
        $eventEntity->setEventType($event->getType());
        $eventEntity->setParameters(json_encode($event->getParametersBag()->toArray()));
        $eventEntity->setVersion('1');

        return $eventEntity;
    }

    /**
     * @param EntityEvent[] $entities
     *
     * @return DomainEvent[]
     */
    public function mapEntitiesToDomain(array $entities)
    {
        $eventFactory = new EventFactory();

        $domainEvents = [];

        foreach ($entities as $entity) {
            $domainEvents[] = $eventFactory->create(
                $entity->getEventType(),
                json_decode($entity->getParameters(), true)
            );
        }

        return $domainEvents;
    }
}
