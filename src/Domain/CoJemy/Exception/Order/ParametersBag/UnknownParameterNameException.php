<?php

namespace Domain\CoJemy\Exception\Order\ParametersBag;

class UnknownParameterNameException extends \Exception
{
}
