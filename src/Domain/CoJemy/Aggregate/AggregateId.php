<?php

namespace Domain\CoJemy\Aggregate;

class AggregateId
{
    /**
     * @var string
     */
    private $id;

    /**
     * @param string $id
     */
    private function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return AggregateId
     */
    public static function generate()
    {
        return new self(uniqid());
    }

    /**
     * @param string $id
     * @return AggregateId
     */
    public static function fromString($id)
    {
        return new self($id);
    }

    /**
     * @return string
     */
    public function __toString()
    {
       return $this->id;
    }
}
