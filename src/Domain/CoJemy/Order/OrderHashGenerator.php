<?php

namespace Domain\CoJemy\Order;

class OrderHashGenerator
{
    public function generate()
    {
        $hashHolder = HashHolder::createFromHashes(md5(uniqid('admin', true)), md5(uniqid('participant', true)));

        return $hashHolder;
    }
}
