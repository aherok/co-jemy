<?php

namespace Domain\CoJemy\Order;

use Domain\CoJemy\Order\Prices\Price;
use Domain\CoJemy\Order\Prices\Total;
use Domain\CoJemy\Order\Prices\Type;

class Prices
{
    /**
     * @var Price
     */
    private $pricePerPackage;

    /**
     * @var Price
     */
    private $deliveryCost;

    /**
     * @param int $pricePerPackage
     * @return Prices
     */
    public function addPricePerPackage($pricePerPackage)
    {
        $modifiedPrices = new Prices();
        $modifiedPrices->pricePerPackage = new Price(Type::pricePerPackage(), $pricePerPackage);
        $modifiedPrices->deliveryCost = $this->deliveryCost;

        return $modifiedPrices;
    }

    /**
     * @param int $deliveryCost
     * @return Prices
     */
    public function addDeliveryCost($deliveryCost)
    {
        $modifiedPrices = new Prices();
        $modifiedPrices->deliveryCost = new Price(Type::deliveryCost(), $deliveryCost);
        $modifiedPrices->pricePerPackage = $this->pricePerPackage;

        return $modifiedPrices;
    }

    /**
     * @return Price
     */
    public function getPricePerPackage()
    {
        return $this->pricePerPackage;
    }

    /**
     * @return Price
     */
    public function getDeliveryCost()
    {
        return $this->deliveryCost;
    }
}
