<?php

namespace Domain\CoJemy\Order\Events;

use Domain\CoJemy\Event;
use Domain\CoJemy\Order\ParametersBag;

class OrderClosedEvent implements Event
{
    private $aggregateId;

    public function __construct($aggregateId)
    {
        $this->aggregateId = $aggregateId;
    }

    public function getType()
    {
        return 'OrderClosedEvent';
    }

    public function getParametersBag()
    {
        $parameters = new ParametersBag();

        $parameters->setParameter('aggregateId', $this->aggregateId);

        return $parameters;
    }

    /**
     * @param array $parameters
     *
     * @return OrderClosedEvent
     */
    public static function fromParameters(array $parameters)
    {
        return new self(
            $parameters['aggregateId']
        );
    }
}
