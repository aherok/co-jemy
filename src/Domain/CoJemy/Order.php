<?php

namespace Domain\CoJemy;

use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\Order\Events\OrderClosedEvent;
use Domain\CoJemy\Order\Events\OrderOpenedEvent;
use Domain\CoJemy\Order\Prices;
use Domain\CoJemy\Order\Status;
use Domain\CoJemy\Order\HashHolder;

class Order extends Aggregate
{
    private $supplierId;

    /**
     * @var Status
     */
    private $status;
    private $hashHolder;

    /**
     * @var Prices
     */
    private $prices;

    public function __construct(AggregateId $aggregateId)
    {
        parent::__construct($aggregateId);
        $this->prices = new Prices();
    }

    /**
     * @param string $aggregateId
     * @param Event[] $events
     * @return Order
     */
    public static function recreate($aggregateId, array $events)
    {
        $order = new self(AggregateId::fromString($aggregateId));

        foreach($events as $event) {
            $order->apply($event);
        }

        return $order;
    }

    /**
     * @param $supplierId
     * @param HashHolder $hashHolder
     * @param $pricePerPackage
     * @param $deliverCost
     */
    public function open($supplierId, HashHolder $hashHolder, $pricePerPackage, $deliverCost)
    {
        $event = new OrderOpenedEvent((string) $this->id, $supplierId, $hashHolder->toArray(), $pricePerPackage, $deliverCost);
        $this->latestEvents[] = $event;
        $this->apply($event);
    }

    /**
     * @param OrderOpenedEvent $event
     */
    protected function applyOrderOpenedEvent(OrderOpenedEvent $event)
    {
        $bag = $event->getParametersBag();

        $this->supplierId = $bag->getParameter('supplierId');
        $this->prices = $this->prices->addPricePerPackage($bag->getParameter('pricePerPackage'));
        $this->prices = $this->prices->addDeliveryCost($bag->getParameter('deliveryCost'));
        
        $hashes = $bag->getParameter('hashes');
        
        $this->hashHolder = HashHolder::createFromHashes($hashes['adminHash'], $hashes['participantHash']);
        
        $this->status = Status::opened();
    }

    protected function applyOrderClosedEvent(OrderClosedEvent $event)
    {
        $this->status = Status::closed();
    }
    
    public function getSupplierId()
    {
        return $this->supplierId; 
    }

    /**
     * @return Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return Prices
     */
    public function getPrices()
    {
        return $this->prices;
    }

    public function close()
    {
        $event = new OrderClosedEvent((string) $this->id);
        $this->latestEvents[] = $event;
        $this->apply($event);
    }
    
    public function getHashHolder()
    {
        return $this->hashHolder;
    }
}
