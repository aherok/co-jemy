<?php

namespace Infrastructure\CoJemy\Order\Handlers;

use Domain\CoJemy\Aggregate\AggregateId;
use Domain\CoJemy\EventStorePersister;
use Domain\CoJemy\Order;
use Infrastructure\CoJemy\Order\Commands\OpenOrderCommand;

class OpenOrderHandler
{
    private $eventStorePersister;
    private $hashGenerator;

    public function __construct(EventStorePersister $eventStorePersister, Order\OrderHashGenerator $hashGenerator)
    {
        $this->eventStorePersister = $eventStorePersister;
        $this->hashGenerator = $hashGenerator;
    }

    public function handleOpenOrderCommand(OpenOrderCommand $command)
    {
        $hashHolder = $this->hashGenerator->generate();
        $order = new Order(AggregateId::generate());
        
        $order->open(
            $command->getSupplierId(),
            $hashHolder,
            $command->getPricePerPackage(),
            $command->getDeliveryCost()
        );
        
        $this->eventStorePersister->persist($order->getLatestEvents());
    }
}
