<?php

namespace Infrastructure\CoJemy\Order\Commands;

class OpenOrderCommand
{
    private $supplierId;
    private $pricePerPackage;
    private $deliveryCost;

    public function __construct($supplierId, $pricePerPackage, $deliveryCost)
    {
        $this->supplierId = $supplierId;
        $this->pricePerPackage = $pricePerPackage;
        $this->deliveryCost = $deliveryCost;
    }

    public function getSupplierId()
    {
        return $this->supplierId;
    }

    public function getPricePerPackage()
    {
        return $this->pricePerPackage;
    }

    public function getDeliveryCost()
    {
        return $this->deliveryCost;
    }
}
