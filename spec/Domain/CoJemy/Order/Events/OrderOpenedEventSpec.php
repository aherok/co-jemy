<?php

namespace spec\Domain\CoJemy\Order\Events;

use Domain\CoJemy\Order\ParametersBag;
use Domain\CoJemy\Order\HashHolder;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Domain\CoJemy\Event;

class OrderOpenedEventSpec extends ObjectBehavior
{
    function let()
    {
        $hashHolder = HashHolder::createFromHashes('adminHash123', 'participantHash123');
        
        $this->beConstructedWith('id123', 'shama', $hashHolder->toArray(), 100.00, 200.00);
    }

    function it_returns_the_event_type()
    {
        $this->getType()->shouldReturn('OrderOpenedEvent');
    }

    function it_is_an_event()
    {
        $this->shouldImplement(Event::class);
    }

    function it_returns_event_parameters()
    {
        $expectedParametersBag = new ParametersBag();
        $expectedParametersBag->setParameter('aggregateId', 'id123');
        $expectedParametersBag->setParameter('supplierId', 'shama');
        $expectedParametersBag->setParameter('pricePerPackage', 100.00);
        $expectedParametersBag->setParameter('deliveryCost', 200.00);
        $expectedParametersBag->setParameter('hashes', ['adminHash' => 'adminHash123', 'participantHash' => 'participantHash123']);

        $this->getParametersBag()->shouldBeLike($expectedParametersBag);
    }
}
