<?php

namespace spec\Domain\CoJemy\Order;

use Domain\CoJemy\Order\Prices;
use Domain\CoJemy\Order\Prices\Price;
use Domain\CoJemy\Order\Prices\Type;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class PricesSpec extends ObjectBehavior
{
    function it_returns_prices_with_delivery_cost()
    {
        $prices = $this->addDeliveryCost(100.00);
        $prices->shouldBeAnInstanceOf(Prices::class);
        $prices->getDeliveryCost()->shouldBeLike(new Price(Type::deliveryCost(), 100.00));
    }

    function it_returns_prices_with_price_per_package()
    {
        $prices = $this->addPricePerPackage(100.00);
        $prices->shouldBeAnInstanceOf(Prices::class);
        $prices->getPricePerPackage()->shouldBeLike(new Price(Type::pricePerPackage(), 100.00));
    }
}
