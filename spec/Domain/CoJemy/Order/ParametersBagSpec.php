<?php

namespace spec\Domain\CoJemy\Order;

use Domain\CoJemy\Exception\Order\ParametersBag\UnknownParameterNameException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ParametersBagSpec extends ObjectBehavior
{
    function it_returns_false_when_parameter_does_not_exist()
    {
        $this->hasParameter('unknown_parameter')->shouldReturn(false);
    }

    function it_returns_true_when_parameter_exists()
    {
        $this->setParameter('some_parameter', 'parameter_value');
        $this->hasParameter('some_parameter')->shouldReturn(true);
    }

    function throws_an_exception_when_trying_to_get_not_existing_parameter()
    {
        $this->shouldThrow(UnknownParameterNameException::class)->during('getParameter', ['unknown_parameter']);
    }

    function it_returns_parameter_value_when_parameter_exists()
    {
        $this->setParameter('some_parameter', 'parameter_value');
        $this->getParameter('some_parameter')->shouldReturn('parameter_value');
    }
}
