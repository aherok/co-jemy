# No to co jemy ?

### Co trzeba aby jeść ?
 - Docker
 - VirtualBox

### Jak jeść ?

Jeżeli nie masz aktywnej maszyny dockera:

```
$ docker-machine create co-jemy --driver vritualbox
```

```
$ eval "$(docker-machine env co-jemy)"
```

```
$ docker-machine ip co-jemy
```

Po wykonaniu komend znamy już IP maszyny - aplikacja będzie dostępna pod tym adresem.

Do /etc/exports dodajemy linię:

```
$ "/Users/twoj-uzytkownik/katalog-z-projektem" -mapall=twoj-uzytkownik:staff ip-maszyny"
```

```
$ docker-machine ssh co-jemy
```

```
$ cd /var/lib/boot2docker
$ sudo vi bootlocal.sh

$ sudo umount /Users
$ sudo mkdir -p /Users/twoj-user/co-jemy
$ sudo /usr/local/etc/init.d/nfs-client start
$ sudo mount 192.168.99.1:/Users/twoj-user/co-jemy /Users/twoj-user/co-jemy -o rw,async,noatime,rsize=32768,wsize=32768,proto=tcp,nfsvers=3

$ sudo chmod 755 bootlocal.sh
$ exit
```

```
$ sudo nfsd restart
```

```
$ docker-machine stop co-jemy
$ docker-machine start co-jemy
```

Przechodzimy do katalogu z aplikacją.

```
$ docker-compose up -d --build web
```

Po zbudowaniu należy odpalić migracje.

```
$ docker-compose run web /bin/bash -c "composer dev"
```

Aplikacja powinna być dostępna pod adresem:

```
http://ip-maszyny/app_dev.php/
```

### Jak testować jedzenie ?

```
$ docker-compose run web bin/behat
```

```
$ docker-compose run web bin/phpspec run
```
