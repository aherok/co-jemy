<?php

namespace Features\Application\Page\FoodSupplier;

use SensioLabs\Behat\PageObjectExtension\PageObject\Page;

class EditFoodSupplierPage extends Page
{
    protected $path = '/food-suppliers/{id}/edit';

    protected $elements = [
        'Food supplier form' => '#food-supplier-form'
    ];

    /**
     * @param array $parameters
     * @throws \Behat\Mink\Exception\ElementNotFoundException
     */
    public function fillFoodSupplierForm(array $parameters)
    {
        $form = $this->getElement('Food supplier form');
        $form->fillField('food_supplier[name]', $parameters['name']);
        $form->fillField('food_supplier[deliveryCost]', $parameters['deliveryCost']);
        $form->fillField('food_supplier[freeDeliveryThreshold]', $parameters['freeDeliveryThreshold']);
        $form->fillField('food_supplier[singlePackageCost]', $parameters['singlePackageCost']);
        $form->fillField('food_supplier[phoneNumber]', $parameters['phoneNumber']);
        $form->fillField('food_supplier[websiteUrl]', $parameters['websiteUrl']);
        $form->fillField('food_supplier[menuUrl]', $parameters['menuUrl']);

        $form->submit();
    }
}
