<?php

namespace Features\Application\Page\FoodSupplier;

use SensioLabs\Behat\PageObjectExtension\PageObject\Page;

class FoodSuppliersListPage extends Page
{
    protected $path = '/food-suppliers/';

    protected $elements = [
        'Suppliers list' => '#food-suppliers',
        'Supplier name' => '#food-suppliers td.food-supplier-name',
        'Supplier delivery cost' => '#food-suppliers td.food-supplier-delivery-cost',
        'Supplier free delivery threshold' => '#food-suppliers td.food-supplier-free-delivery-threshold',
        'Supplier single package cost' => '#food-suppliers td.food-supplier-single-package-cost',
        'Supplier phone number' => '#food-suppliers td.food-supplier-phone-number',
        'Supplier website url' => '#food-suppliers td.food-supplier-website-url',
        'Supplier menu url' => '#food-suppliers td.food-supplier-menu-url'
    ];

    public function hasFoodSuppliers()
    {
        return $this->hasElement('Suppliers list');
    }
    
    public function checkForSupplierWithParameters(array $parameters)
    {
        $expectations = [
            'name' => $this->getElement('Supplier name')->getText(),
            'deliveryCost' => $this->getElement('Supplier delivery cost')->getText(),
            'freeDeliveryThreshold' => $this->getElement('Supplier free delivery threshold')->getText(),
            'singlePackageCost' => $this->getElement('Supplier single package cost')->getText(),
            'phoneNumber' => $this->getElement('Supplier phone number')->getText(),
            'websiteUrl' => $this->getElement('Supplier website url')->getText(),
            'menuUrl' => $this->getElement('Supplier menu url')->getText()
        ];
        
        foreach ($expectations as $expectationName => $expectation) {
            if ($expectation !== $parameters[$expectationName]) {
                throw new \RuntimeException(sprintf('Expected %s %s , got %s', $expectationName, $parameters[$expectationName], $expectation));
            }
        }
    }
    
}
