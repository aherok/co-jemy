@food-supplier
Feature: Updating food suppliers
  In order to keep food suppliers up to date
  As an administrator
  I have to be able to update them

  Scenario: Updating food supplier
    Given the database is empty
    And there is food supplier with parameters:
      | name       | deliveryCost | freeDeliveryThreshold | singlePackageCost | phoneNumber | websiteUrl          | menuUrl                 |
      | supplier-1 | 10.00        | 50.00                 | 5.00              | 123-456-789 | http://supplier.com | http://some-menu-url.pl |
    When I am on "/food-suppliers/1/edit"
    And I fill supplier edit form with parameters:
      | name       | deliveryCost  | freeDeliveryThreshold  | singlePackageCost  | phoneNumber | websiteUrl                 | menuUrl                        |
      | supplier-3 | 100.00        | 500.00                 | 50.00              | 666-666-666 | http://supplier-edited.com | http://some-menu-url-edited.pl |
    Then I should be on food supplier page
    And there should be supplier with parameters:
      | name       | deliveryCost  | freeDeliveryThreshold  | singlePackageCost  | phoneNumber | websiteUrl                 | menuUrl                        |
      | supplier-3 | 100.00        | 500.00                 | 50.00              | 666-666-666 | http://supplier-edited.com | http://some-menu-url-edited.pl |
