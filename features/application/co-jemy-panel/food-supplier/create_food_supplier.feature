@food-supplier
Feature: Creating new food supplier
  In order to be able to select food supplier
  As an administrator
  I have to be able to create food supplier

  Scenario: Create food supplier
    Given the database is empty
    When I am on "/food-suppliers/new"
    And I fill supplier add form with parameters:
      | name       | deliveryCost | freeDeliveryThreshold | singlePackageCost | phoneNumber | websiteUrl          | menuUrl                 |
      | supplier-1 | 10.00        | 50.00                 | 5.00              | 123-456-789 | http://supplier.com | http://some-menu-url.pl |
    Then I should be on food supplier page
    And there should be supplier with parameters:
      | name       | deliveryCost | freeDeliveryThreshold | singlePackageCost | phoneNumber | websiteUrl          | menuUrl                 |
      | supplier-1 | 10.00        | 50.00                 | 5.00              | 123-456-789 | http://supplier.com | http://some-menu-url.pl |
