@domain
Feature: Open new order
  As a user I want to...
  So that I...

  Scenario:
    Given there are no orders in the system
    When I open new order for supplier "shama" with price per package 100.00 and delivery cost 200.00
    Then there should be "1" order with status "opened"
    And order should be placed for supplier "shama"
    And order admin hash should not be null
    And order participant hash should not be null
    And order should have delivery cost set to 200.00 and price per package to 100.00
